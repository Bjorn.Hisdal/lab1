package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random rand = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> yesOrNo = Arrays.asList("y", "n");

    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String computerPick = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            String humanPick = readInput(("Your choice (Rock/Paper/Scissors)?"), rpsChoices);
            String choiceString = "Human chose " + humanPick + ", computer chose " + computerPick + ".";

            if (isWinner(humanPick, computerPick)) {
                System.out.println(choiceString + " Human wins!");
                humanScore ++;
            }
            else if (isWinner(computerPick, humanPick)) {
                System.out.println(choiceString + " Computer wins!");
                computerScore ++;
            }
            else {
                System.out.println(choiceString + " It's a tie!");
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            roundCounter ++;
            String continueAnswer = readInput(("Do you wish to continue playing? (y/n)?"), yesOrNo);
            if (continueAnswer.equals("n")) {
                break;
            }
        }
        System.out.println("Bye bye :)");
    }

    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt, List<String> givenList) {
        while (true) {
            System.out.println(prompt);
            String userInput = sc.next().toLowerCase();
            boolean answer = givenList.contains(userInput);
            if (answer) {
                return userInput;
            }
            else {
                System.out.println("I don't understand " + userInput + ". Could you try again?");
            }    
        }
    }

    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("rock")) {
            return choice2.equals("scissors");
        }
        else if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else {
            return choice2.equals("paper");
        }
    }
}
